<?php

$router = new Phalcon\Mvc\Router();

/*
  $router->add("/:controller/:action/:params", array(
  "controller" => 1,
  "action" => 2,
  "params" => 3,
  ));
 */
//peticiones get
$router->addGet("/robots", array(
    "controller" => "robots",
    "action" => "index",
        #"params" => 2,
));

$router->addGet("/robots/{id:[0-9]+}", array(
    "controller" => "robots",
    "action" => "search",
    "params" => 1,
));

$router->addGet('/robots/searchname/{name}', array(
    "controller" => "robots",
    "action" => "searchname",
    "params" => 3,
));

#$router->addPost('/robots/{name: [a-z]{4}}/ {type: [a-z]{4}} / {year: [0-9]{4}}', array(
#$router->addPost('/robots/{name: [a-z]{4}}/ {type: [a-z]{4}} / {year: [0-9]{4}}', array(
#$router->addPost('/robots/add/name/[a-z]{4}/type: [a-z]{4}/year: [0-9]{4}', array(
$router->addPost('/robots/add/name/{name}/type/{type}/year/{year}', array(
    "controller" => "robots",
        #"action" => "add",
        #"name" => 1,
        #"type" => 2,
        #"year" => 3,
));

/*
  //peticiones post
  $router->addPost("/robots/{id:[0-9]+", array(
  "controller" => "robots",
  "params" => 2,
  ));
 */
return $router;



















/*
  $app->notFound(function () use ($app) {
  $app->response->setStatusCode(404, "Not Found")->sendHeaders();
  echo 'This is crazy, but this page was not found!';
  });

#$robots = new Phalcon\Mvc\Micro\Collection();
#$robots->setHandler(new RobotsController());
#$robots->setPrefix('/robots');
#print_r($robots->get('/robots/', 'index'));
#$robots->get('/robots/', 'index');
#$app->mount($robots);

/*
  //Retrieves all robots
  $myController = new RobotsController();
  $app->get('robots', array($myController, "indexAction"));
  //Searches for robots with $name in their name
  $app->get('/robots/searchname/{name}', array($myController, "searchnameAction"));
  //Retrieves robots based on primary key
  $app->get('/robots/{id:[0-9]+}', array($myController, "searchAction"));
  //Adds a new robot
  $app->post('/robots/name/{name}/type/{type}/year/{year}', array($myController, "addAction"));
  //Deletes robots based on primary key
  $app->delete('/robots/{id:[0-9]+}', array($myController, "deleteAction"));

  //Updates robots based on primary key
  #$app->put('/robots/{id:[0-9]+}' use ($app) ,array($myController, "updateAction") );
  #$app->put('/robots/{id:[0-9]+}',use ($app), array($myController, "updateAction") );
  $app->handle();
 */