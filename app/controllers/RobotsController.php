<?php

#namespace Controllers;

/**
 * @RoutePrefix("/api-rest/robots")
 */
class RobotsController extends ControllerBase {

    public function indexAction() {
        #print("hola");exit;
        $query = Robots::find();
        foreach ($query as $robot) {
#print_r($robot->id);exit;
            $data[] = array(
                'id' => $robot->id,
                'name' => $robot->name,
                'type' => $robot->type,
                'year' => $robot->year
            );
        }
        $response = array('status' => 'OK', 'robots' => $data);
        echo json_encode($response);
    }

    public function searchnameAction($name) {
        #print_r("ogog");exit;
        $robots = Robots::find("name= '$name'");
        $data = array();
        foreach ($robots as $robot) {
            $data[] = array(
                'id' => $robot->id,
                'name' => $robot->name,
            );
        }
        echo json_encode($data);
    }

    public function searchAction($id) {
        $robot = Robots::findFirst($id);
        if ($robot == false) {
            $response = array('status' => 'NOT-FOUND');
        } else {
            $response = array(
                'status' => 'FOUND',
                'data' => array(
                    'id' => $robot->id,
                    'name' => $robot->name
                )
            );
        }
        echo json_encode($response);
    }

    public function addAction($name, $type, $year) {
        #print_r($name . " " . $type . " " . $year);exit;
        $robots = new Robots();
        $robots->name = $name;
        #print_r($name);exit;
        $robots->type = $type;
        $robots->year = $year;
        if ($robots->save() == false) {//$robots->save(); return 1 o 0
            $this->response->setStatusCode(409, "Conflict")->sendHeaders();
            $errors = array();
            foreach ($robots->getMessages() as $message) {
                $errors[] = "$message";
                #echo "Message: ", $message->getMessage();
                #echo "Field: ", $message->getField();
                #echo "Type: ", $message->getType();
            }
            $response = array('status' => "Error", "data" => $errors);
        } else {
            $this->response->setStatusCode(201, "Created")->sendHeaders();
            #$robots->id = Robots::getModel()->id;
            $response = array('status' => 'OK', 'data' => $robots);
        }
        echo json_encode($response);
    }

    public function deleteAction($id) {
        $robot = Robots::findFirst($id);
        if ($robot != false) {
            if ($robot->delete() == false) {
                $errors = array();
                foreach ($robot->getMessages() as $message) {
                    $errors[] = "$message";
                }
                $this->response->setStatusCode(409, "Conflict")->sendHeaders();
                $response = array('status' => 'ERROR', 'data' => $errors);
            } else {
                $response = array('status' => 'OK');
            }
        } else {
            $response = array('status' => 'ERROR', 'data' => "Not found id: $id");
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function updateAction($id) {
        #print_r("hola");
        #exit;
        $request = new \Phalcon\Http\Request();
        $robot = $request->getRawBody();
        #$robot = json_decode($app->request->getRawBody());
        print_r($robot);
        exit;
        $phql = "DELETE FROM Robots WHERE id = :id:";
        $status = $app->modelsManager->executeQuery($phql, array(
            'id' => $id
        ));
        if ($status->success() == true) {
            $response = array('status' => 'OK');
        } else {
            //Change the HTTP status
            $this->response->setStatusCode(409, "Conflict")->sendHeaders();
            $errors = array();
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $response = array('status' => 'ERROR', 'messages' => $errors);
        }
        echo json_encode($response);
    }

}
