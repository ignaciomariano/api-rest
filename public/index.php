<?php


header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *"); #PERMITE LLAMADAS DESDE OTRAS URl
error_reporting(E_ALL);
ini_set('display_errors', '1');


try {

    /* Read the configuration */
    $config = include __DIR__ . "/../app/config/config.php";
    /* Read auto-loader */
    include __DIR__ . "/../app/config/loader.php";
    /* Read services */
    include __DIR__ . "/../app/config/services.php";

    /* Handle the request */
    #$app = new \Phalcon\Mvc\Micro();
    #$app = new \Phalcon\Mvc\Micro($di);
    #$app->handle();

    $application = new \Phalcon\Mvc\Application($di);
    $application->useImplicitView(false); //Use to ignore View
    #$application->handle()->send();
    echo $application->handle()->getContent();
    #$loader = new \Phalcon\Loader();
    #$loader->registerDirs(array(__DIR__ . '/../app/controllers/'))->register();
    #$loader->registerDirs(array(__DIR__ . '/../app/models/'))->register();
} catch (\Exception $e) {
    echo $e->getMessage();
}
